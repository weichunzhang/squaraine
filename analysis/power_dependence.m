close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
%% Read data
folder = '..\data\power_dependence\';

bg = dlmread(strcat(folder,'Power_dependence_Sample=1uM_K8-1342_MeOH_776nm_circular_lambda=776nm_Meas_at_18hs54m_TimeTrace_Power=0uW.dat'),',',2,0);

bkg_mean = mean(bg)*100;
bkg_std = std(bg)*100;

file = dir(strcat(folder,'Power_dependence_Sample=1uM_K8-1342_MeOH_776nm_circular_lambda=776nm_Meas_at_18hs54m_averaged.dat'));

a = csvread(strcat(folder,file.name),1,0);
pw = a(:,1); % power at bfp

c2 = a(:,3); % counter 2
e2 = a(:,5);

ch2 = c2 - bkg_mean;
e = sqrt(e2.^2+bkg_std.^2); % propagation of uncertainty.

errorbar(pw,ch2,e,'sb','LineWidth',LineWidth);
hold all
%% fit
log_power = log(pw); % Power for fitting
log_cnt = log(ch2); % counts for fitting.

[p_poly,S] = polyfit(log_power,log_cnt,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict = polyval(p_poly,log_power);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power),exp(cnt_predict),'b--','LineWidth',LineWidth);

hold all

%% pulsed laser.

A = dlmread(strcat(folder,'Power_dependence_Sample=NR771_5uM_K8-1342_MeOH_NR0_lambda=774nm_Meas_at_18hs49m_TimeTrace_Power=0uW.dat'),',',1,0);
B = dlmread(strcat(folder,'Power_dependence_Sample=NR771_5uM_K8-1342_MeOH_NR0_lambda=774nm_Meas_at_18hs49m_TimeTrace_Power=20.01uW.dat'),',',1,0);
C = dlmread(strcat(folder,'Power_dependence_Sample=NR771_5uM_K8-1342_MeOH_NR0_lambda=774nm_Meas_at_18hs49m_TimeTrace_Power=96.92999999999999uW.dat'),',',1,0);
D = dlmread(strcat(folder,'Power_dependence_Sample=NR771_5uM_K8-1342_MeOH_NR0_lambda=774nm_Meas_at_18hs49m_TimeTrace_Power=122.24uW.dat'),',',1,0);
E = dlmread(strcat(folder,'Power_dependence_Sample=NR771_5uM_K8-1342_MeOH_NR0_lambda=774nm_Meas_at_18hs49m_TimeTrace_Power=240.20000000000002uW.dat'),',',1,0);

dark = mean(A(2,:))*100;
std_dark = std(A(2,:))*100;
count = [100*mean(B(2,:))-dark 100*mean(C(2,:))-dark 100*mean(D(2,:))-dark 100*mean(E(2,:))-dark]/5; % cps, convert to 1 uM
std_count = [sqrt((std_dark/5).^2+(100*std(B(2,:))/5).^2) sqrt((std_dark/5).^2+(100*std(C(2,:)/5)).^2) sqrt((std_dark/5).^2+(100*std(D(2,:))/5).^2) sqrt((std_dark/5).^2+(100*std(E(2,:))/5).^2)];
power = [20.01 96.92999999999999 122.24 240.20000000000002]/80;

errorbar(power,count,std_count,'or','LineWidth',LineWidth);

%% fit
log_power_1 = log(power); % Power for fitting
log_cnt_1 = log(count); % counts for fitting.

[p_poly_1,S_1] = polyfit(log_power_1,log_cnt_1,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict_1 = polyval(p_poly_1,log_power_1);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power_1),exp(cnt_predict_1),'r--','LineWidth',LineWidth);

xlim([0.2 30]);
ylim([30 2e4]);

set(gca,'yscale','log')
set(gca,'xscale','log')
legend('Data, CW excitation',strcat('{Fit. Slope = }',num2str(p_poly(1))),'Data, pulsed excitation',strcat('{Fit. Slope = }',num2str(p_poly_1(1))));
legend('location','southeast');
legend('boxoff');
xlabel('Excitation power(uW)')
ylabel('Fluorescence (counts/s)')
% set(gca,'xtick',[1e-2 1e-1 1 10 100]);
% set(gca,'ytick',[1e2 1e3 1e4 1e5 1e6]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)

% grid
if ifsave == 1
    % saveas(gcf, '..\images\seta670_power_dependence', 'png')
    saveas(gcf, '..\images\seta670_power_dependence', 'svg')
end