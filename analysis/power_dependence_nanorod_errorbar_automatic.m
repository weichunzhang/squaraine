close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
FontName = 'SansSerif';
folder = '..\data\power_dependence\';

NR1 = 'NR5';
NR2 = 'NR6';
%% The first nanorod.

file_zero1 = dir(strcat(folder,'Power_dependence_Sample=NR771_MeOH_',NR1,'_lambda=774nm_Meas_at_*_TimeTrace_Power=0uW.dat'));
zero1 = dlmread(strcat(folder,file_zero1.name),',',1,0);
dark1 = mean(zero1(2,:))*100;
std_dark1 = std(zero1(2,:))*100;

power1 = zeros(1,4);
count1 = zeros(1,4);
std_count1 = zeros(1,4);

% power nonzero.
files1 = dir(strcat(folder,'Power_dependence_Sample=NR771_MeOH_',NR1,'_lambda=774nm_Meas_at_*_TimeTrace_Power=*.*uW.dat'));
for M =1:size(files1,1)
    aux1 = strfind(files1(M).name,'=');
    aux2 = strfind(files1(M).name,'u');
    power1(M) = str2double(files1(M).name(aux1(3)+1:aux2-1))/80;
    temp = dlmread(strcat(folder,files1(M).name),',',1,0);
    count1(M) = 100*mean(temp(2,:))-dark1;
    std_count1(M) = sqrt(std_dark1.^2+(100*std(temp(2,:))).^2); % error propagation.
end

errorbar(power1,count1,std_count1,'ob','LineWidth',LineWidth);
hold all
%% fit
log_power1 = log(power1); % Power for fitting
log_cnt1 = log(count1); % counts for fitting.

[p_poly1,S1] = polyfit(log_power1,log_cnt1,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict1 = polyval(p_poly1,log_power1);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power1),exp(cnt_predict1),'b--','LineWidth',LineWidth);

%% Another rod.

file_zero2 = dir(strcat(folder,'Power_dependence_Sample=NR771_MeOH_',NR2,'_lambda=774nm_Meas_at_*_TimeTrace_Power=0uW.dat'));
zero2 = dlmread(strcat(folder,file_zero2.name),',',1,0);
dark2 = mean(zero2(2,:))*100;
std_dark2 = std(zero2(2,:))*100;

power2 = zeros(1,4);
count2 = zeros(1,4);
std_count2 = zeros(1,4);

% power nonzero.
files2 = dir(strcat(folder,'Power_dependence_Sample=NR771_MeOH_',NR2,'_lambda=774nm_Meas_at_*_TimeTrace_Power=*.*uW.dat'));
for N =1:size(files2,1)
    aux1 = strfind(files2(N).name,'=');
    aux2 = strfind(files2(N).name,'u');
    power2(N) = str2double(files2(N).name(aux1(3)+1:aux2-1))/80;
    temp = dlmread(strcat(folder,files2(N).name),',',1,0);
    count2(N) = 100*mean(temp(2,:))-dark2;
    std_count2(N) = sqrt(std_dark2.^2+(100*std(temp(2,:))).^2); % error propagation.
end

errorbar(power2,count2,std_count2,'sr','LineWidth',LineWidth);
hold all
%% fit
log_power2 = log(power2); % Power for fitting
log_cnt2 = log(count2); % counts for fitting.

[p_poly2,S2] = polyfit(log_power2,log_cnt2,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict2 = polyval(p_poly2,log_power2);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power2),exp(cnt_predict2),'r--','LineWidth',LineWidth);

%% plot settings

% ylim([1e1 1e6]);

set(gca,'yscale','log');
set(gca,'xscale','log');
legend('Nanorod 1',strcat('{Fit. Slope = }',num2str(p_poly1(1))),'Nanorod 2',strcat('{Fit. Slope = }',num2str(p_poly2(1))));
legend('location','southeast');
xlabel('Excitation power(muW)');
ylabel('Photoluminescence (counts/s)');
xlim([0.2 3.5]);
set(gca,'xtick',[0.2 0.4 0.6 0.8 1 2 3]);
% set(gca,'ytick',[1e2 1e3 1e4 1e5 1e6]);

set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);
set(gca,'Fontname','SansSerif');
set(gca,'FontSize',FontSize);
set(gca,'linewidth',Border);

% grid
if ifsave == 1
    %saveas(gcf, strcat('..\images\power_dependence_',NR1,'_',NR2), 'png')
    saveas(gcf, '..\images\power_dependence_nanorod', 'svg');
end
