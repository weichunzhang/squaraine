clc
close all
clear 
folder = '..\data\spectra\';

LineWidth= 1.5;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 1;

A = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_5_5degrees.csv'),',',2,0);
B = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_20degrees.csv'),',',2,0);
D = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_40degrees.csv'),',',2,0);
E = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_60degrees.csv'),',',2,0);
ind = find(A(:,1) >=660 & A(:,1)<=765);

plot(A(ind,1),A(ind,2),'b',B(ind,1),B(ind,2),'r',D(ind,1),D(ind,2),'g',E(ind,1),E(ind,2),'m','LineWidth',LineWidth);
xlabel('Wavelength (nm)')
ylabel('FUCL intensity (a.u.)')
% title('100 \muM K8-1342 in MeOH, 775 nm excitation')
xlim([660 765]);
ylim([-10 120]);
legend('278.5 K','293 K','313 K','333 K')
legend('location','northwest')

set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf,'..\images\emission_spectra_antistokes_temperature_dependence','svg');
end
