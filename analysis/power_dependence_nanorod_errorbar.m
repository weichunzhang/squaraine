close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 0;

folder = '..\data\power_dependence\';

%% The first nanorod.
A = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR3_lambda=774nm_Meas_at_16hs46m_TimeTrace_Power=0uW.dat'),',',1,0);
B = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR3_lambda=774nm_Meas_at_16hs46m_TimeTrace_Power=19.548uW.dat'),',',1,0);
C = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR3_lambda=774nm_Meas_at_16hs46m_TimeTrace_Power=94.99uW.dat'),',',1,0);
D = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR3_lambda=774nm_Meas_at_16hs46m_TimeTrace_Power=119.19uW.dat'),',',1,0);
E = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR3_lambda=774nm_Meas_at_16hs46m_TimeTrace_Power=243.2uW.dat'),',',1,0);

dark = mean(A(2,:))*100;
std_dark = std(A(2,:))*100;
count = [100*mean(B(2,:))-dark 100*mean(C(2,:))-dark 100*mean(D(2,:))-dark 100*mean(E(2,:))-dark]/5; % cps, convert to 1 uM
std_count = [sqrt((std_dark/5).^2+(100*std(B(2,:))/5).^2) sqrt((std_dark/5).^2+(100*std(C(2,:)/5)).^2) sqrt((std_dark/5).^2+(100*std(D(2,:))/5).^2) sqrt((std_dark/5).^2+(100*std(E(2,:))/5).^2)];
power = [19.548 94.99 119.19 243.2]/80;

errorbar(power,count,std_count,'ob','LineWidth',LineWidth);
hold all
%% fit
log_power = log(power); % Power for fitting
log_cnt = log(count); % counts for fitting.

[p_poly,S] = polyfit(log_power,log_cnt,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict = polyval(p_poly,log_power);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power),exp(cnt_predict),'b--','LineWidth',LineWidth);

%% Another rod.

A = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR4_lambda=774nm_Meas_at_16hs50m_TimeTrace_Power=0uW.dat'),',',1,0);
B = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR4_lambda=774nm_Meas_at_16hs50m_TimeTrace_Power=20.11uW.dat'),',',1,0);
C = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR4_lambda=774nm_Meas_at_16hs50m_TimeTrace_Power=92.08999999999999uW.dat'),',',1,0);
D = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR4_lambda=774nm_Meas_at_16hs50m_TimeTrace_Power=123.17uW.dat'),',',1,0);
E = dlmread(strcat(folder,'Power_dependence_Sample=NR771_MeOH_NR4_lambda=774nm_Meas_at_16hs50m_TimeTrace_Power=243.0uW.dat'),',',1,0);

dark = mean(A(2,:))*100;
std_dark = std(A(2,:))*100;
count = [100*mean(B(2,:))-dark 100*mean(C(2,:))-dark 100*mean(D(2,:))-dark 100*mean(E(2,:))-dark]/5; % cps, convert to 1 uM
std_count = [sqrt((std_dark/5).^2+(100*std(B(2,:))/5).^2) sqrt((std_dark/5).^2+(100*std(C(2,:)/5)).^2) sqrt((std_dark/5).^2+(100*std(D(2,:))/5).^2) sqrt((std_dark/5).^2+(100*std(E(2,:))/5).^2)];
power = [20.11 92.08999999999999 123.17 243.0]/80;

errorbar(power,count,std_count,'or','LineWidth',LineWidth);

%% fit
log_power_1 = log(power); % Power for fitting
log_cnt_1 = log(count); % counts for fitting.

[p_poly_1,S_1] = polyfit(log_power_1,log_cnt_1,1); % mu improves the numerical properties of both the polynomial and the fitting algorithm.
cnt_predict_1 = polyval(p_poly_1,log_power_1);
% yresid = log_cnt - cnt_predict; % Redidual
% SSresid = sum(yresid.^2);
% SStotal = (length(log_cnt)-1) * var(log_cnt);
% rsq = 1 - SSresid/SStotal; % R squared.
plot(exp(log_power_1),exp(cnt_predict_1),'r--','LineWidth',LineWidth);

%% plot settings

xlim([0.2 3.5]);
% ylim([30 2e4]);

set(gca,'yscale','log')
set(gca,'xscale','log')
legend('Nanorod 3',strcat('{Fit. Slope = }',num2str(p_poly(1))),'Nanorod 4',strcat('{Fit. Slope = }',num2str(p_poly_1(1))));
legend('location','southeast')
xlabel('Excitation power(\muW)')
ylabel('Fluorescence (counts/s)')
% set(gca,'xtick',[1e-2 1e-1 1 10 100]);
% set(gca,'ytick',[1e2 1e3 1e4 1e5 1e6]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)

% grid
if ifsave == 1
    saveas(gcf, '..\images\saturation_curve', 'png')
    saveas(gcf, '..\images\saturation_curve', 'svg')
end
