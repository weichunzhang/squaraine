clear
close all
clc

LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
FontName = 'SansSerif';
%% Plot image
x = 0:0.1:30;
y = 0:0.1:30;
A = dlmread('../data/images/NR771_MeOH_774nm_circular_3uW.dat',',',4,0); % From 2017-05-10.
imagesc(x,y,A*10);
c = colorbar;
c.Label.String = 'counts/(10 ms)';
colormap hot
% xlabel('x (\mum)');
% ylabel('y (\mum)');
axis image

hold all
% scale bar
plot([3 8],[28 28],'LineWidth',LineWidth,'color','white');
text(3,26.5,'5 um','color','white');

xlim([0 30]);
ylim([0 30]);
set(gca,'xtick',[]);
set(gca,'ytick',[]);
set(gca,'Fontname','SansSerif');
set(gca,'FontSize',FontSize);
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf, '..\images\NR_PL_image_matlab', 'svg')
end