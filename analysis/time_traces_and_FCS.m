close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
FontName = 'SansSerif';
%% Read data
folder1 = '..\data\time_traces\';
folder2 = '..\data\FCS\';

trace1 = dlmread(strcat(folder1,'solution_1uM_K8-1342_MeOH_fs_1_5uW_circular_time_trace_10ms.dat'),'',1,0);
trace2 = dlmread(strcat(folder1,'in_MeOH_1.5uW_NR9_timetrace_10ms.dat'),'',1,0);

plot(trace1(:,1),trace1(:,2),'b',trace2(:,1),trace2(:,2),'r','LineWidth',1);
xlim([0 60]);
xlabel('Time (s)');
ylabel('Counts/ms');
set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);
if ifsave == 1
    saveas(gcf,'..\images\enhanced_time_trace','svg');
end


figure
FCS1 = dlmread(strcat(folder2,'solution_1uM_K8-1342_MeOH_fs_1_5uW_circular_FLCS.dat'),'',1,0);
FCS2 = dlmread(strcat(folder2,'in_MeOH_1.5uW_NR9_FCS.dat'),'',1,0);

semilogx(FCS1(:,1),FCS1(:,2),'b',FCS2(:,1),FCS2(:,2),'r.','LineWidth',LineWidth,'MarkerSize',10);
hold all

beta0 = [0.1 2];
beta = nlinfit(FCS2(:,1),FCS2(:,2),'single_exp_tau',beta0);
FCS_fit = single_exp_tau(beta,FCS2(:,1));
plot(FCS2(:,1),FCS_fit,'r','LineWidth',LineWidth);

xlabel('Time (ms)')
ylabel('g(t)-1');
xlim([0.02 200]);
set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
set(gca,'xtick',[1e-1 1e0 1e1 1e2]);
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);
if ifsave == 1
    saveas(gcf,'..\images\enhanced_FCS','svg');
end