clc
clear
close all

A = xlsread('Z:\Weichun\Measurements\2017-11-15_Ensemble_enhancement_of_R6G_in_MeOH\Results\SPR_versus_enhancement_factor_no_NR13.xlsx',1,'B2:E31');

errorbar(A(:,1),(A(:,3)-1)*2000,A(:,4)*2000,'o'); % Considering the volume ratio
xlabel('SPR wavelength (nm)');
ylabel('Enhancement');
% hold all

% wl = 700:2:830;
% beta0 = [8000,770,20];
% [beta,R,J,CovB] = nlinfit(A(:,1),(A(:,3)-1)*2000,'gauss_1d',beta0);
% factor_fit = gauss_1d(beta,wl);
% plot(wl,factor_fit);
% 
% beta_ci = nlparci(beta,R,'jacobian',J,'alpha',0.05); % Confidence interval
% c_wl_ci = beta_ci(2,2)-beta(2);
% sigma_ci = beta_ci(3,2)-beta(3);
% FWHM = 2.355*beta(3)
% FWHM_ci = sigma_ci*2.355
xlim([710 820]);
% text(790,8000,'FWHM = 38.7 nm');

