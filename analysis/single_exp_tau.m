function y = single_exp_tau(beta,x)
    y = beta(1)*exp(-x/beta(2));