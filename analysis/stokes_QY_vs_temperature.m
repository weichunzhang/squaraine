clc
clear
close all
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
FontName = 'SansSerif';

% load('quantum_yield_vs_T');
load('quantum_yield_vs_T_complete'); % with more temperatures.

plot(QY_complete(:,1),QY_complete(:,2),'bo',...
    'LineWidth',LineWidth,'MarkerSize',8);
xlabel('Temperature (K)');
ylabel('Stokes QY');
% title('1 \muM K8-1342 in MeOH');
ylim([0 0.4]);
xlim([270 340]);
hold all
%% exp fit
beta0 = [100 40];
beta = nlinfit(QY_complete(:,1),QY_complete(:,2),'single_exp_tau',beta0);
T_fit = 275:1:335;
QY_fit = single_exp_tau(beta,T_fit);
plot(T_fit,QY_fit,'--r','LineWidth',LineWidth);

set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf,'..\images\stokes_QY_vs_temperature_matlab','svg');
end