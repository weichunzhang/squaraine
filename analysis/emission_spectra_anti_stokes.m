close all
clear
clc
%% plot settings
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
FontName = 'SansSerif';

%% Reading data
folder = '..\data\spectra\';

A = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage.csv'),',',2,0);
B = dlmread(strcat(folder,'K8-1342_100uM_MeOH_785nm_exc_high_voltage.csv'),',',2,0);

%% plot
plot(A(:,1),A(:,2),'b',B(:,1),B(:,2),'r','LineWidth',LineWidth);
xlabel('Wavelength (nm)')
ylabel('Intensity (a.u.)')

xlim([620 900]);
ylim([-5 700]);
legend('775 nm','785 nm')
legend('location','northwest');
legend('boxoff');

set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

% grid
if ifsave == 1
    % saveas(gcf, '..\images\seta670_power_dependence', 'png')
    saveas(gcf, '..\images\emission_spectra_anti_stokes', 'svg')
end

%% Excitation wavelength dependent intensity.
wl_low = 675;
wl_high = 765;
ind = find(A(:,1)>=wl_low & A(:,1)<=wl_high);
int_775 = trapz(A(ind,1),A(ind,2));
int_785 = trapz(B(ind,1),B(ind,2));
ratio = int_775/int_785

