clear
close all
clc

%% Use only xz and yz scan.
%% x
x1 = 237.0;
x1_err = 1.4;
x2 = 259.4;
x2_err = 1.1;

x = (x1+x2)/2;
x_err = sqrt((x1_err)/2^2+(x2_err/2)^2);

%% z
z1 = 731.1;
z1_err = 4.4;
z2 = 736.4;
z2_err = 3.1;

z = (z1+z2)/2;
z_err = sqrt((z1_err/2)^2+(z2_err/2)^2);

Vconf = (pi/2)^1.5*x*x*z
Vconf_err = Vconf*sqrt((x_err/x)^2 + (x_err/x)^2 + (z_err/z)^2)

Veff = pi^1.5*x*x*z
Veff_err = Veff*sqrt((x_err/x)^2 + (x_err/x)^2 + (z_err/z)^2)

%% Calculation of the molecular brightness.
folder = '..\data\time_traces\';
dark = load(strcat(folder,'dark.t3r.dat'));
Seta = load(strcat(folder,'Solution_circular.t3r.dat'));

ip_dark = diff(dark); % interphoton times.
[mu_dark, ci_dark] = expfit(ip_dark);
count_dark = 1./mu_dark; % convert interphoton time to count rate.
err_dark = (ci_dark(2)-mu_dark)./(mu_dark.^2);

ip_Seta = diff(Seta);
[mu_Seta, ci_Seta] = expfit(ip_Seta);
count_Seta = 1./mu_Seta; % convert interphoton time to count rate.
err_Seta = (ci_Seta(2)-mu_Seta)./(mu_Seta.^2);

Seta = count_Seta - count_dark; % net count rate from the dye.
err_Seta = sqrt(err_dark^2+err_Seta^2);

c = 1000; % concentration, nM
NA = 6.02214086e23; % avogadro constant
N = c*1e-9*Vconf*1e-24*NA; % number of molecules in the focal volume.
N_err = c*1e-9*Vconf_err*1e-24*NA; % uncertainty.

mo_br = Seta/N % molecular brightness
mo_br_err = mo_br*sqrt((err_Seta/Seta)^2 + (N_err/N)^2);


