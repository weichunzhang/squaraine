% test spectra load

close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\');
folder = '..\data\spectra\';

LineWidth= 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 1;

%% Single NR spectrum

low = 600;
high = 832;
BG1 = load(strcat(folder,'NR771_MeOH_532nm_20uW_circular_NR_0.txt'),'txt');
ind1 = find(BG1(:,1)>low&BG1(:,1)<high);
wl = BG1(ind1,1);
BG = BG1(ind1,2);

NR7 = load(strcat(folder,'NR771_MeOH_532nm_20uW_circular_NR_16.txt'),'txt');
NR7_net = NR7(ind1,2)-BG;

% Response function
corr = load(strcat(folder,'spectral_response_20171027.txt'),'txt');
ind2 = find(corr(:,1)>low&corr(:,1)<high);
response = corr(ind2,2);

NR7_corr = NR7_net./response; % The measured spectra and the spectrum for correction have the same range.

%% bluk NR data
a = csvread(strcat(folder,'Nanopartz_A12-40-780-CTAB-replacement.csv'),2,0);
w = a(:,1);
S = a(:,2)/max(a(find(a(:,1)>400),2));

clear a
plot(w,S,'k','LineWidth',LineWidth);
% h(1).FaceColor = [218,165,32]/255;
% title('Single Rod Luminiscence')
hold all
% end
% h(1).FaceAlpha = 0.5;
clear w s


%% Spectra of K8-1342
% absorption
data = csvread(strcat(folder,'1uM_K8-1342_MeOH_abs.csv'),2,0);
wa=data(:,1);
a=data(:,2);
% emission
data1 = csvread(strcat(folder,'1uM_K8-1342_MeOH_635nm_exc_medium_vol_25degree_1.csv'),2,0);
data2 = csvread(strcat(folder,'1uM_K8-1342_MeOH_635nm_exc_medium_vol_25degree_2.csv'),2,0);
we=data1(:,1);
e=(data1(:,2)+data2(:,2))/2;
clear data

%% Spectra of C102


%%
figure(1)

plot(wa,a./max(a),'b-.',we,e./max(e),'r--',wl,NR7_corr/max(NR7_corr),'g','LineWidth',LineWidth);
hold all
% plot(wa,a./max(a),'color',[0 0.5 1],'LineWidth',LineWidth);
plot([775 775],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line
ylim([0 1.05])
xlim([400 900]);

% labels and axis
% set(gcf,'units','centimeters')
% set(gcf,'position',[8 8 16 9])
% % set(gcf,'position',[482   214   956   815])

% l=legend('GNR bulk extinction','Single GNR luminescence','Seta 670 absorption','Seta 670 emission');
% % legend('boxoff');
% set(l,'location','northwest')
set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf, '..\images\K8-1342_and_NR_spectra_matlab', 'svg');
end
