% test spectra load

close all
clear
clc

addpath('Z:\Weichun\Matlab_programs\Spectra\');
folder = '..\data\spectra\';

LineWidth= 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 0;

%% Spectra of K8-1342
% absorption
data = csvread(strcat(folder,'1uM_K8-1342_MeOH_abs.csv'),2,0);
wa=data(:,1);
a=data(:,2);
% emission
data1 = csvread(strcat(folder,'1uM_K8-1342_MeOH_635nm_exc_medium_vol_25degree_1.csv'),2,0);
data2 = csvread(strcat(folder,'1uM_K8-1342_MeOH_635nm_exc_medium_vol_25degree_2.csv'),2,0);
we=data1(:,1);
e=(data1(:,2)+data2(:,2))/2;
clear data

%%
plot(wa,a./max(a),'b',we,e./max(e),'g--','LineWidth',LineWidth);
% plot(wa,a./max(a),'color',[0 0.5 1],'LineWidth',LineWidth);
% plot([775 775],[-0.1 1.1],':k','LineWidth',LineWidth) % laser line
ylim([0 1.05])
xlim([500 900]);

hold all

%% 100 uM high concentration.
data3 = csvread(strcat(folder,'K8-1342_100uM_MeOH_600nm_exc_medium_voltage.csv'),2,0);
plot(data3(:,1),data3(:,2)/max(data3(:,2)),'r-.','LineWidth',LineWidth);

set(gca,'Fontname','SansSerif')
set(gca,'LineWidth',Border)
set(gca,'FontSize',FontSize)
xlabel('Wavelength (nm)','FontSize',FontSize)
ylabel('Normalized intenstiy ','FontSize',FontSize)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf, '..\images\K8-1342_spectra_matlab', 'svg');
end
