clc
close all
clear

%% plot settings
LineWidth = 1.5;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 1;

spectrum = load('..\data\spectra\S1704_01_NR771_water_774nm_3uW_APD2_NR_5.txt','txt');
plot(spectrum(:,1),spectrum(:,2)-270,'LineWidth',LineWidth);
xlabel('Wavelength (nm)');
ylabel('PL intensity (a.u.)');
xlim([550 770]);
ylim([-10 900]);
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);
set(gca,'LineWidth',Border);
set(gca,'Fontname','SansSerif');
set(gca,'FontSize',FontSize);

if ifsave == 1
    saveas(gcf, '..\images\two_photon_luminescence_spectrum_nanorod', 'svg');
end