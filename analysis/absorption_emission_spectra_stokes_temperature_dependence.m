%% Absorpton and emission spectra at different temperatures.

clc
close all
clear 
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 1;
FontName = 'SansSerif';

folder = '..\data\spectra\';

%% absorption
abs = dlmread(strcat(folder,'K8-1342_1uM_MeOH_5_5degree_1.csv'),',',2,0);
wl_abs = abs(:,1);
abs1 = abs(:,2);
abs2 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_5_5degree_2.csv'),',',2,1);
abs3 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_5_5degree_3.csv'),',',2,1);
abs4 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_5_5degree_4.csv'),',',2,1);
% abs_5 = (abs1(:,1)+abs2(:,1)+abs3(:,1)+abs4(:,1))/4;
abs_5 = abs4(:,1);

abs5 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_20degree_1.csv'),',',2,1);
abs6 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_20degree_2.csv'),',',2,1);
abs7 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_20degree_3.csv'),',',2,1);
% abs_20 = (abs5(:,1)+abs6(:,1)+abs7(:,1))/3;
abs_20 = abs5(:,1);

abs8 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_40degree_1.csv'),',',2,1);
abs9 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_40degree_2.csv'),',',2,1);
abs10 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_40degree_3.csv'),',',2,1);
% abs_40 = (abs8(:,1)+abs9(:,1)+abs10(:,1))/3;
abs_40 = abs8(:,1);

abs11 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_60degree_1.csv'),',',2,1);
abs12 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_60degree_2.csv'),',',2,1);
abs13 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_60degree_3.csv'),',',2,1);
abs14 = dlmread(strcat(folder,'K8-1342_1uM_MeOH_60degree_4.csv'),',',2,1);
% abs_60 = (abs11(:,1)+abs12(:,1)+abs13(:,1)+abs14(:,1))/4;
abs_60 = abs14(:,1);

ind1 = find(wl_abs>500);
 plot(wl_abs,abs_5/max(abs_5(ind1)),'b',wl_abs,abs_20/max(abs_5(ind1)),'r',wl_abs,abs_40/max(abs_5(ind1)),'g',wl_abs,abs_60/max(abs_5(ind1)),'m',...
    'LineWidth',LineWidth);
% plot(wl_abs,abs_5,'b',wl_abs,abs_20,'r',wl_abs,abs_40,'g',wl_abs,abs_60,'m',...
%    'LineWidth',LineWidth);

hold all
%% Emission

A = dlmread(strcat(folder,'K8_1342_MeOH_1uM_650nm_exc_medium_voltage_5_5degree.csv'),',',2,0);
B = dlmread(strcat(folder,'K8_1342_MeOH_1uM_650nm_exc_medium_voltage_20degree.csv'),',',2,0);
C = dlmread(strcat(folder,'K8_1342_MeOH_1uM_650nm_exc_medium_voltage_40degree.csv'),',',2,0);
D = dlmread(strcat(folder,'K8_1342_MeOH_1uM_650nm_exc_medium_voltage_60degree.csv'),',',2,0);
ind = find(A(:,1)<900);

plot(A(ind,1),A(ind,2)/max(A(ind,2)),'b--',B(ind,1),B(ind,2)/max(A(ind,2)),'r--',C(ind,1),C(ind,2)/max(A(ind,2)),'g--',D(ind,1),D(ind,2)/max(A(ind,2)),'m--','LineWidth',LineWidth);
xlabel('Wavelength (nm)')
ylabel('Normalized intensity')
legend('278.5 K abs','293 K abs','313 K abs','333 K abs','278.5 K em','293 K em','313 K em','333 K em');
xlim([500 850]);
ylim([-0.02 1.02]);
legend('boxoff');
legend('location','northwest');
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf,'..\images\absorption_emission_spectra_stokes_temperature_dependence_matlab','svg');
end

%% Zoom in
figure
plot(wl_abs,abs_5,'b',wl_abs,abs_20,'r',wl_abs,abs_40,'g',wl_abs,abs_60,'m',...
    'LineWidth',LineWidth);
xlim([630 665]);
xlabel('Wavelength (nm)')
ylabel('Optical density')
legend('278.5 K','293 K','313 K','333 K');
% ylim([-0.02 1.02]);
legend('boxoff');
legend('location','northwest');
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf,'..\images\absorption_spectra_stokes_temperature_dependence_zoom_matlab','svg');
end


