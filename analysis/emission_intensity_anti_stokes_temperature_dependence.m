clc
close all
clear 
folder = '..\data\spectra\';

LineWidth= 2;
Border = 2;
FontSize = 16;
FontName = 'SansSerif';
ifsave = 0;

A = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_5_5degrees.csv'),',',2,0);
B = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_20degrees.csv'),',',2,0);
C = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_25degrees.csv'),',',2,0);
D = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_30degrees.csv'),',',2,0);
E = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_40degrees.csv'),',',2,0);
F = dlmread(strcat(folder,'K8-1342_100uM_MeOH_775nm_exc_high_voltage_60degrees.csv'),',',2,0);

ind = find(A(:,1) >=675 & A(:,1)<=765);

%% plot temperature dependence, QY corrected.
QY = load('quantum_yield_vs_T.mat');
int_5_5 = trapz(A(ind,1),A(ind,2))/QY.QY_output(1,2);
int_20 = trapz(B(ind,1),B(ind,2))/QY.QY_output(2,2);
int_25 = trapz(C(ind,1),C(ind,2))/QY.QY_output(3,2);
int_30 = trapz(D(ind,1),D(ind,2))/QY.QY_output(4,2);
int_40 = trapz(E(ind,1),E(ind,2))/QY.QY_output(5,2);
int_60 = trapz(F(ind,1),F(ind,2))/QY.QY_output(6,2);

intensity = [int_5_5 int_20 int_25 int_30 int_40 int_60];
T = [5.5 20 25 30 40 60] + 273;


% %% Exponential fit without sigma.
% beta0 = 1e7;
% beta = nlinfit(T',intensity','single_exp1',beta0);
% T_fit = 270:1:340;
% int_fit = single_exp1(beta,T_fit);
% plot(T_fit,int_fit);
% legend('Data','I = A exp(-\DeltaE/kT)');
% legend('location','northwest');
% 
% figure
% plot(1./T,log(intensity),'o',1./T_fit,log(int_fit));
% xlabel('1/T (K^{-1})');
% ylabel('ln(intensity)');
% legend('Data','I = A exp(-\DeltaE/kT)');

% %% Exponential fit, sigma
% figure
% beta0 = [1e7 1];
% beta = nlinfit(T',intensity','single_exp2',beta0);
% T_fit = 270:1:340;
% int_fit = single_exp2(beta,T_fit);
% plot(T,intensity,'o',T_fit,int_fit);
% legend('Data','I = A exp(-\sigma\DeltaE/kT)');
% legend('location','northwest');
% xlabel('Temperature (K)');
% ylabel('Intensity (a.u.)');
% title('100 \muM K8-1342 in MeOH, exc @ 775 nm');
% 
% figure
% plot(1./T,log(intensity),'o',1./T_fit,log(int_fit));
% xlabel('1/T (K^{-1})');
% ylabel('ln(intensity)');
% legend('Data','I = A exp(-\sigma\DeltaE/kT)');

%% Nonlinear fit is ill-conditioned, try linear fit, sigma. single_exp1 and single_exp2 are not needed.
figure
kb = 1.38064852e-23; % Boltzmann constant
c = 299792458;
h = 6.62607004e-34;
lam_exc = 775e-9;
lam_em = 715e-9;
delta_E = h*c*(1/lam_em - 1/lam_exc); % The expected activation energy

p = polyfit(1./(kb*T),log(intensity),1);
Delta_E = -p(1); % fitted activation energy

T_fit = 275:1:335;
int_fit_log = polyval(p,1./(kb*T_fit));
int_fit = exp(int_fit_log);
plot(T,intensity,'ob',T_fit,int_fit,'r','LineWidth',LineWidth,'MarkerSize',8);
% legend('Data','I = A exp(-\sigma\DeltaE/kT)');
% legend('location','northwest');
xlabel('Temperature (K)');
ylabel('Anti-Stokes abs. (a.u.)');
ylim([0 3.5e4])
% title('100 \muM K8-1342 in MeOH, exc @ 775 nm');
% set(gca,'ytick',[]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);
xlim([270 340]);

if ifsave == 1
    saveas(gcf,'..\images\emission_intensity_anti_stokes_temperature_dependence_exp','svg');
end

% straight line.
figure
plot(1./T,log(intensity),'ob',1./T_fit,int_fit_log,'r','LineWidth',LineWidth,'MarkerSize',8);
xlabel('1/T (K^{-1})');
ylabel('ln(anti-Stokes) (a.u.)');
% xlim([T_fit(1) T_fit(end)]);
% legend('Data','I = A exp(-\sigma\DeltaE/kT)');
xlim([1/340 1/270]);
set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf,'..\images\emission_intensity_anti_stokes_temperature_dependence_linear','svg');
end

