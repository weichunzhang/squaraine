clc
close all
clear 
LineWidth = 2;
Border = 2;
FontSize = 16;
ifsave = 0;
FontName = 'SansSerif';

folder = '..\data\spectra\';

A = dlmread(strcat(folder,'K8-1342_100nM_MeOH_600nm_exc_high_voltage.csv'),',',2,0);
B = dlmread(strcat(folder,'K8-1342_1uM_MeOH_600nm_exc_high_voltage.csv'),',',2,0);
C = dlmread(strcat(folder,'K8-1342_10uM_MeOH_600nm_exc_medium_voltage.csv'),',',2,0);
D = dlmread(strcat(folder,'K8-1342_100uM_MeOH_600nm_exc_medium_voltage.csv'),',',2,0);
ind = find(A(:,1)<900);

plot(A(ind,1),A(ind,2)/max(A(ind,2)),B(ind,1),B(ind,2)/max(B(ind,2)),C(ind,1),C(ind,2)/max(C(ind,2)),D(ind,1),D(ind,2)/max(D(ind,2)),'LineWidth',LineWidth);
xlabel('Wavelength (nm)')
ylabel('Normalized intensity')

xlim([640 850]);
ylim([-0.02 1.02]);
legend('0.1 uM','1 uM','10 uM','100 uM');
legend('boxoff');

set(gca,'Fontname','SansSerif')
set(gca,'FontSize',FontSize)
set(gca,'linewidth',Border)
set(findall(gcf,'-property','FontSize'),'FontSize',FontSize,'FontName',FontName);

if ifsave == 1
    saveas(gcf,'..\images\emission_spectra_stokes_concentration_dependence','svg');
end