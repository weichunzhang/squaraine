 \documentclass[11pt,a4paper,onecolumn]{article}
% \documentclass[options]{class}
% Example: \documentclass[11pt,twoside,a4paper]{article} 10 pt is by default.

\setlength{\columnsep}{1cm}
 
% \usepackage{epstopdf}
\usepackage[T1]{fontenc}       % Use modern font encodings
% \usepackage[version=3]{mhchem} % Formula subscripts using \ce{}
 \usepackage{graphicx}
\usepackage[a4paper,top=2cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}
% \usepackage{authblk} % For affiliation. This package is not preinstalled.
\graphicspath{{../final_images/}}
\newcommand*\commentauthor[1]{\textbf{{\textit{#1}}}}
\newcommand*\me[1]{\ensuremath{\bar{#1}\,}}
\newcommand*\chem[1]{\ensuremath{\mathrm{#1}}}

\newcommand*{\affaddr}[1]{#1} % No op here. Customize it for different styles.
\newcommand*{\affmark}[1][*]{\textsuperscript{#1}}
\newcommand*{\email}[1]{\texttt{#1}} % These three commands are for author affilations.

\linespread{1.3} % Line spacing. 1.3 means one-and-a-half spacing.

\begin{document}

\author{
Weichun Zhang, Mart\'in Caldarola, Michel Orrit\\\affaddr{Huygens-Kamerlingh Onnes Laboratory, Leiden University, 2300 RA Leiden, Netherlands}\\\email{orrit@physics.leidenuniv.nl}
}

\date{\vspace{1ex}} % Exclude date in the title.
%\affiliation
 %\affil{Huygens-Kamerlingh Onnes Laboratory, Leiden University, 2300 RA Leiden, Netherlands} % This is only useful for authblk package.
 %\email{orrit@leidenuniv.nl}
\title{\textbf{Plasmonic enhancement of two-photon-excited fluorescence by individual gold nanorods}\\ \vspace{3ex} Supplementary Information \vspace{3ex}}
\maketitle
\tableofcontents
\pagebreak
%%

% \section{Size of the confocal volume}

% \section{Supplementary data}

\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth,keepaspectratio]{absorption_spectra_stokes_temperature_dependence_zoom}
	\makeatletter
	\renewcommand{\fnum@figure}{\figurename~S\thefigure}
	\makeatother
  \caption{\textbf{Detailed absorbance spectra of 1 $\mu$M Seta 670 as a function of temperature}.
	}
  \label{fg:zoom}
\end{figure}


\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth,keepaspectratio]{emission_spectra_stokes_concentration_dependence}
	\makeatletter
	\renewcommand{\fnum@figure}{\figurename~S\thefigure}
	\makeatother
  \caption{\textbf{Concentration dependence of the Stokes fluorescence intensity of Seta 670}.
	}
  \label{fg:conc_dep}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth,keepaspectratio]{emission_spectra_anti_stokes_775nm_785nm}
	\makeatletter
	\renewcommand{\fnum@figure}{\figurename~S\thefigure}
	\makeatother
  \caption{FUCL emission spectra of Seta 670 (100 $\mu$M in methanol) excited at 775 nm and 785 nm. The high peaks at the excitation wavelengths were from the scattering of the solution.
	}
  \label{fg:lambda_dep}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth,keepaspectratio]{emission_intensity_anti_stokes_temperature_dependence_linear}
	\makeatletter
	\renewcommand{\fnum@figure}{\figurename~S\thefigure}
	\makeatother
  \caption{Linear plots of logarithm of integrated fluorescence intensity against inverse
temperature of Seta 670 solution.
	}
  \label{fg:linear_plot}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=1\columnwidth,keepaspectratio]{nanorod_two_photon_spectrum_power_dependence_image}
	\makeatletter
	\renewcommand{\fnum@figure}{\figurename~S\thefigure}
	\makeatother
  \caption{\textbf{Two-photon-excited luminescence of gold nanorods}. (a) Typical two-photon-excited luminescence spectrum of a gold nanorod immersed in water. The immobilized nanorod was excited with the femtosecond laser (circularly polarized) with an average power of 3 $\mu$W.  (b) Log-log plot of the dependence of the photoluminescence intensity on the excitation power for two gold nanorods. The fitted slopes reveal a quadratic intensity-excitation relation, confirming the two-photon excitation origin of photoluminescence from the nanorods. The nanorods were immersed in methanol and excited with the femtosecond laser (circularly polarized). (c) An image of two-photon-excited luminescence of gold nanorods immobilized on a glass coverslip and immersed in methanol. The image size is 30 $\mu$m $\times$ 30 $\mu$m. Each pixel is 0.2 $\mu$m $\times$ 0.2 $\mu$m with an integration time of 10 ms/pixel. The excitation source was the femtosecond laser (circularly polarized) with an average power of 3 $\mu$W. For (a-c), a 745-nm shortpass filter and a 785-nm notch filter were used to block the excitation wavelength.
	}
  \label{fg:2p_PL_NR}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth,keepaspectratio]{PSF_xz}
	\makeatletter
	\renewcommand{\fnum@figure}{\figurename~S\thefigure}
	\makeatother
  \caption{\textbf{\textit{xz} section of the scattering PSF measured with a gold nanorod}. Line profiles through the center are shown. The red (along the \textit{x} axis) and green (along the \textit{z} axis) dots show the experimental data and the black lines represent two-dimensional Gaussian fits, which yield $w_x = 237.0 \pm 1.4$ nm and $w_z = 731.1 \pm 4.4$ nm.
	}
  \label{fg:PSF}
\end{figure}

We measured the size of the confocal volume by measuring the point spread function (PSF) of the microscope.\cite{PSF_fitting} We scanned sectional scattering images of an immobilized gold nanorod immersed in index matched objective oil excited with the Ti:Sapphire laser. No detection filters were used. Figure S\ref{fg:PSF} shows the \textit{xz} section of the PSF (\textit{z} is along the optical axis). All three sections (\textit{xy}, \textit{xz} and \textit{yz}) of the PSF were fitted with two-dimensional Gaussian functions as described in Chapter 2. The size of the confocal volume is then calculated from the mean dimensions in each axis as
\begin{equation}
V_\mathrm{conf} = \Big ( \frac{\pi}{2} \Big) ^{3/2} w_{x}w_{y}w_{z} = 0.089 \pm 0.005 \; \mathrm{fL},
\label{eq:confocal_volume}
\end{equation}
where $w_x$, $w_y$ and $w_z$ are the \(1/e^2\) radii along \textit{x}, \textit{y} and \textit{z} axis, respectively.

\pagebreak
\bibliographystyle{ieeetr} % If I use acm style, the bib doens't start from [1]. For a list of bibliography styles, see https://www.sharelatex.com/learn/Bibtex_bibliography_styles
\bibliography{sup_info}


\end{document}